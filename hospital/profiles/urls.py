from rest_framework import routers

from profiles.views import DoctorViewSet, PatientViewSet, SpecialityViewSet, ScheduleViewSet, AppointmentViewSet

router = routers.DefaultRouter()
router.register(r'doctors', DoctorViewSet)
router.register(r'patients', PatientViewSet)
router.register(r'schedule', ScheduleViewSet)
router.register(r'appointments', AppointmentViewSet)
router.register(r'specialities', SpecialityViewSet)
router.register(r'speciality-doctor', SpecialityViewSet)

urlpatterns = []
