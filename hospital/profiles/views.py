from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, permissions

from profiles.models import Doctor, Patient, Speciality, SpecialityDoctor, Schedule, Appointment
from profiles.serializer import DoctorSerializer, PatientSerializer, SpecialitySerializer, SpecialityDoctorSerializer, \
    ScheduleSerializer, AppointmentSerializer


class DoctorViewSet(viewsets.ModelViewSet):
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer
    permission_classes = [permissions.IsAuthenticated]


class PatientViewSet(viewsets.ModelViewSet):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
    permission_classes = [permissions.IsAuthenticated]


class ScheduleViewSet(viewsets.ModelViewSet):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer
    permission_classes = [permissions.IsAuthenticated]


class AppointmentViewSet(viewsets.ModelViewSet):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer
    permission_classes = [permissions.IsAuthenticated]


class SpecialityViewSet(viewsets.ModelViewSet):
    queryset = Speciality.objects.all()
    serializer_class = SpecialitySerializer
    permission_classes = [permissions.IsAuthenticated]


class SpecialityDoctorViewSet(viewsets.ModelViewSet):
    queryset = SpecialityDoctor.objects.all()
    serializer_class = SpecialityDoctorSerializer
    permission_classes = [permissions.IsAuthenticated]
