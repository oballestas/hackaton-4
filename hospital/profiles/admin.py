from django.contrib import admin

# Register your models here.
from profiles.models import Patient, Doctor, Speciality, SpecialityDoctor, Schedule, Appointment


@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    list_display = ('id', 'dni', 'name', 'last_name', 'email')


@admin.register(Patient)
class PatientAdmin(admin.ModelAdmin):
    list_display = ('id', 'dni', 'name', 'last_name')


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('id', 'appointment_date', 'start_time', 'end_time', 'doctor')


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'appointment_date', 'start_time', 'end_time', 'doctor', 'patient')


@admin.register(Speciality)
class SpecialityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


@admin.register(SpecialityDoctor)
class SpecialityDoctorAdmin(admin.ModelAdmin):
    list_display = ('id', 'doctor', 'speciality')
