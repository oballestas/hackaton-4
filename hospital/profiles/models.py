from django.db import models

# Create your models here.
from django.db import models


class RegisterData(models.Model):
    register_date = models.DateField(auto_now_add=True)
    modification_date = models.DateField(auto_now=True)
    register_user = models.CharField(max_length=60)
    modification_user = models.CharField(max_length=60)
    active = models.BooleanField(default=False)

    class Meta:
        abstract = True


class Patient(RegisterData):
    name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    dni = models.CharField(max_length=100, unique=True)
    address = models.CharField(max_length=100)
    telephone = models.IntegerField()
    gender = models.CharField(max_length=40)
    birth_date = models.DateField()

    def __str__(self):
        return f'{self.name} {self.last_name}'


class Doctor(RegisterData):
    name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    dni = models.CharField(max_length=100, unique=True)
    address = models.CharField(max_length=100)
    email = models.EmailField()
    telephone = models.IntegerField()
    gender = models.CharField(max_length=40)
    number_school = models.IntegerField()
    birth_date = models.DateField()

    def __str__(self):
        return f'{self.name} {self.last_name}'


class Schedule(RegisterData):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    appointment_date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()

    def __str__(self):
        return f'{self.appointment_date}'


class Appointment(RegisterData):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    appointment_date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    status = models.CharField(max_length=50)
    observations = models.TextField()

    def __str__(self):
        return f'{self.appointment_date}'


class Speciality(RegisterData):
    name = models.CharField(max_length=30)
    description = models.TextField()

    def __str__(self):
        return self.name


class SpecialityDoctor(RegisterData):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    speciality = models.ForeignKey(Speciality, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.doctor.name} {self.doctor.last_name}'
