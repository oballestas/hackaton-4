import './App.css';
import { Home } from './pages/Home';
import { Navbar } from './components/navbar/Navbar';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { Doctors } from './pages/Doctors';
import { DoctorForm } from './components/doctor_form/doctorForm';
import { DoctorEditForm } from './components/doctor_form/doctorEditForm';
import { DoctorDetail } from './components/doctor_detail/DoctorDetail';
import { Login } from './pages/Login';
import { Patients } from './pages/Patients';
import { PatientForm } from './components/patient_form/PatientForm';
import { PatientEditForm } from './components/patient_form/PatientEditForm';
import { PatientDetail } from './components/patient_detail/PatientDetail';
import { Schedule } from './pages/Schedule';
import { ScheduleForm } from './components/schedule_form/ScheduleForm';
import { ScheduleEditForm } from './components/schedule_form/ScheduleEditForm';
import { ScheduleDetail } from './components/schedule_detail/ScheduleDetail';
import { Appointments } from './pages/Appointments';
import { AppointmentForm } from './components/appointment_form/AppointmentForm';
import { AppointmentEditForm } from './components/appointment_form/AppointmentEditForm';
import { AppointmentDetail } from './components/appointment_detail/AppointmentDetail';

function App() {
  return (
    <div className='App'>
      <Router>
        <Navbar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/login' element={<Login />} />
          <Route path='/doctors' element={<Doctors />} />
          <Route path='/doctors/add' element={<DoctorForm />} />
          <Route path='/doctors/edit/:doctorId' element={<DoctorEditForm />} />
          <Route path='/doctors/detail/:doctorId' element={<DoctorDetail />} />
          <Route path='/patients' element={<Patients />} />
          <Route path='/patients/add' element={<PatientForm />} />
          <Route
            path='/patients/detail/:patientId'
            element={<PatientDetail />}
          />
          <Route
            path='/patients/edit/:patientId'
            element={<PatientEditForm />}
          />
          <Route path='/schedule' element={<Schedule />} />
          <Route path='/schedule/add' element={<ScheduleForm />} />
          <Route
            path='/schedule/edit/:scheduleId'
            element={<ScheduleEditForm />}
          />
          <Route
            path='/schedule/detail/:scheduleId'
            element={<ScheduleDetail />}
          />
          <Route path='/appointments' element={<Appointments />} />
          <Route path='/appointments/add' element={<AppointmentForm />} />
          <Route
            path='/appointments/edit/:appointmentId'
            element={<AppointmentEditForm />}
          />
          <Route
            path='/appointments/detail/:appointmentId'
            element={<AppointmentDetail />}
          />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
