import { useEffect, useState } from 'react';
import axios from 'axios';
import { AppointmentTable } from '../components/appointment_table/AppointmentTable';

export function Appointments() {
  let [dataAppointments, setDataAppointments] = useState([]);
  let [error, setError] = useState('');
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get('http://127.0.0.1:8000/api/appointments/', authConfig)
      .then((respuesta) => {
        setDataAppointments(respuesta.data);
      })
      .catch((err) => {
        setError(err);
      });
  }, []);
  return (
    <div>
      {error.response?.status === 401 ? (
        <h5 className='text-danger mt-5'>
          Please, login to access to this page
        </h5>
      ) : (
        <AppointmentTable {...dataAppointments} />
      )}
    </div>
  );
}
