import axios from 'axios';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

export function Login() {
  let [error, setError] = useState('');
  const navigate = useNavigate();

  function errorManage(error) {
    switch (error.response.status) {
      case 401:
        setError(error.response.data.detail);
        break;
      case 400:
        setError(error.response.data);

        break;

      default:
        break;
    }
  }

  function userLogin(e) {
    console.log('Login');
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());

    axios
      .post('http://127.0.0.1:8000/api/token/', values)
      .then((respuesta) => {
        console.log(respuesta);
        localStorage.setItem('token', respuesta.data.access);
        navigate('/');
        window.location.reload();
      })
      .catch((error) => {
        console.log(error);
        errorManage(error);
      });
  }

  return (
    <div>
      <div className='align-items-center mt-5 pt-5'>
        <div className='d-flex justify-content-center'>
          <form
            onSubmit={(event) => {
              userLogin(event);
            }}
          >
            <div className='mb-4'>
              <label className='form-label'>User</label>
              <input
                type='text'
                className='form-control'
                id='username'
                name='username'
                aria-describedby='emailHelp'
                placeholder='Username'
              />
            </div>
            <div className='mb-3'>
              <label className='form-label'>Password</label>
              <input
                type='password'
                className='form-control'
                id='password'
                name='password'
                placeholder='Password'
              />
            </div>
            <div className='text-center mt-4'>
              <button type='submit' className='btn btn-dark'>
                Login
              </button>
            </div>
            <div className='mt-3'>
              {error.username || error.password ? (
                <div>
                  <p className='text-danger text-center'>
                    {error.username
                      ? `Username error: ${error.username[0]}`
                      : ''}
                  </p>
                  <p className='text-danger text-center'>
                    {error.password
                      ? `Password error: ${error.password[0]}`
                      : ''}
                  </p>
                </div>
              ) : (
                <p className='text-danger text-center'>{error}</p>
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
