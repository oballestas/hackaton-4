import { useEffect, useState } from 'react';
import axios from 'axios';
import { PatientsTable } from '../components/patients_table/PatientsTable';

export function Patients() {
  let [dataPatients, setDataPatients] = useState([]);
  let [error, setError] = useState('');
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get('http://127.0.0.1:8000/api/patients/', authConfig)
      .then((respuesta) => {
        //console.log(respuesta);
        setDataPatients(respuesta.data);
      })
      .catch((err) => {
        setError(err);
      });
  }, []);
  return (
    <div>
      {error.response?.status === 401 ? (
        <h5 className='text-danger mt-5'>
          Please, login to access to this page
        </h5>
      ) : (
        <PatientsTable {...dataPatients} />
      )}
    </div>
  );
}
