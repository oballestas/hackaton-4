import { useEffect, useState } from 'react';
import axios from 'axios';
import { DoctorTable } from '../components/doctor_table/doctorTable';

export function Doctors() {
  let [dataDoctors, setDataDoctors] = useState([]);
  let [error, setError] = useState('');
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get('http://127.0.0.1:8000/api/doctors/', authConfig)
      .then((respuesta) => {
        setDataDoctors(respuesta.data);
      })
      .catch((err) => {
        setError(err);
      });
  }, []);
  return (
    <div>
      {error.response?.status === 401 ? (
        <h5 className='text-danger mt-5'>
          Please, login to access to this page
        </h5>
      ) : (
        <DoctorTable {...dataDoctors} />
      )}
    </div>
  );
}
