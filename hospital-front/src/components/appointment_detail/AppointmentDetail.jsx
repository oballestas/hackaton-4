import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

export function AppointmentDetail() {
  let urlParameters = useParams();
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };
  let [dataAppointment, setDataAppointment] = useState([]);
  let [doctorData, setDoctorData] = useState([]);
  let [patientData, setPatientData] = useState([]);

  function getDoctorData(doctorId) {
    axios
      .get(`http://127.0.0.1:8000/api/doctors/${doctorId}`, authConfig)
      .then((respuesta) => {
        setDoctorData(respuesta.data);
      });
  }

  function getPatientData(patientId) {
    axios
      .get(`http://127.0.0.1:8000/api/patients/${patientId}`, authConfig)
      .then((respuesta) => {
        setPatientData(respuesta.data);
      });
  }

  useEffect(() => {
    axios
      .get(
        `http://127.0.0.1:8000/api/appointments/${urlParameters.appointmentId}`,
        authConfig
      )
      .then((respuesta) => {
        setDataAppointment(respuesta.data);
        getDoctorData(respuesta.data.doctor);
        getPatientData(respuesta.data.patient);
        //console.log(respuesta);
      });
  }, []);
  return (
    <div className='d-flex justify-content-center mt-5 pt-3'>
      <div className='card mx-3 mb-4 mt-2' style={{ width: '400px' }}>
        <div className='text-center mt-3'>
          <span className='text-primary'> id: </span> {dataAppointment.id}
        </div>
        <div className='text-center'>
          <span className='text-primary'> Appointment Date </span>:{' '}
          {dataAppointment.appointment_date}
        </div>

        <div className='text-center'>
          <span className='text-primary'> Start Time </span>:{' '}
          {dataAppointment.start_time}
        </div>
        <div className='text-center'>
          <span className='text-primary'> End Time </span>:{' '}
          {dataAppointment.end_time}
        </div>

        <div className='text-center'>
          <span className='text-primary'> Patient </span>: {patientData.name}{' '}
          {patientData.last_name}
        </div>

        <div className='text-center'>
          <span className='text-primary'> Doctor </span>: {doctorData.name}{' '}
          {doctorData.last_name}
        </div>

        <div className='text-center'>
          <span className='text-primary'>Registration User </span>:{' '}
          {dataAppointment.register_user}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Modification User </span>:{' '}
          {dataAppointment.modification_user}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Register Date </span>:{' '}
          {dataAppointment.register_date}
        </div>
        <div className='text-center mb-3'>
          <span className='text-primary'>Modification Date </span>:{' '}
          {dataAppointment.modification_date}
        </div>
      </div>
    </div>
  );
}
