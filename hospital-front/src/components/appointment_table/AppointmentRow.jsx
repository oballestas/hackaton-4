import axios from 'axios';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

export function AppointmentRow({ id, appointment_date, doctor, patient }) {
  let [appointmentId, setAppointmentId] = useState(id);
  let [doctorData, setDoctorData] = useState([]);
  let [patientData, setPatientData] = useState([]);
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get(`http://127.0.0.1:8000/api/patients/${patient}`, authConfig)
      .then((respuesta) => {
        setPatientData(respuesta.data);
      });
  }, []);

  useEffect(() => {
    axios
      .get(`http://127.0.0.1:8000/api/doctors/${doctor}`, authConfig)
      .then((respuesta) => {
        setDoctorData(respuesta.data);
      });
  }, []);

  function deleteAppointment() {
    axios
      .delete(
        `http://127.0.0.1:8000/api/appointments/${appointmentId}`,
        authConfig
      )
      .then((respuesta) => {
        console.log(respuesta);
        window.location.reload();
      });
  }

  return (
    <tr key={id}>
      <th scope='row'>
        <Link to={`/appointments/detail/${appointmentId}`}>{id}</Link>
      </th>
      <td>{appointment_date}</td>
      <td>
        {doctorData.name} {doctorData.last_name}
      </td>
      <td>
        {patientData.name} {patientData.last_name}
      </td>

      <td>
        <Link className='btn btn-primary mx-3' to={`/appointments/edit/${id}`}>
          Edit
        </Link>

        <button
          className='btn btn-danger'
          type='submit'
          onClick={() => {
            deleteAppointment();
          }}
        >
          Delete
        </button>
      </td>
    </tr>
  );
}
