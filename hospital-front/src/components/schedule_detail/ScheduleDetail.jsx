import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

export function ScheduleDetail() {
  let urlParameters = useParams();
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };
  let [dataSchedule, setDataSchedule] = useState([]);
  let [doctorData, setDoctorData] = useState([]);

  function getDoctorData(doctorId) {
    axios
      .get(`http://127.0.0.1:8000/api/doctors/${doctorId}`, authConfig)
      .then((respuesta) => {
        setDoctorData(respuesta.data);
      });
  }

  useEffect(() => {
    axios
      .get(
        `http://127.0.0.1:8000/api/schedule/${urlParameters.scheduleId}`,
        authConfig
      )
      .then((respuesta) => {
        setDataSchedule(respuesta.data);
        getDoctorData(respuesta.data.doctor);
        //console.log(respuesta);
      });
  }, []);
  return (
    <div className='d-flex justify-content-center mt-5 pt-3'>
      <div className='card mx-3 mb-4 mt-2' style={{ width: '400px' }}>
        <div className='text-center mt-3'>
          <span className='text-primary'> id: </span> {dataSchedule.id}
        </div>
        <div className='text-center'>
          <span className='text-primary'> Appointment Date </span>:{' '}
          {dataSchedule.appointment_date}
        </div>

        <div className='text-center'>
          <span className='text-primary'> Start Time </span>:{' '}
          {dataSchedule.start_time}
        </div>
        <div className='text-center'>
          <span className='text-primary'> End Time </span>:{' '}
          {dataSchedule.end_time}
        </div>

        <div className='text-center'>
          <span className='text-primary'> Doctor </span>: {doctorData.name}{' '}
          {doctorData.last_name}
        </div>

        <div className='text-center'>
          <span className='text-primary'>Registration User </span>:{' '}
          {dataSchedule.register_user}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Modification User </span>:{' '}
          {dataSchedule.modification_user}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Register Date </span>:{' '}
          {dataSchedule.register_date}
        </div>
        <div className='text-center mb-3'>
          <span className='text-primary'>Modification Date </span>:{' '}
          {dataSchedule.modification_date}
        </div>
      </div>
    </div>
  );
}
