import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

export function DoctorDetail() {
  let urlParameters = useParams();
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };
  let [dataDoctor, setDataDoctor] = useState([]);
  useEffect(() => {
    axios
      .get(
        `http://127.0.0.1:8000/api/doctors/${urlParameters.doctorId}`,
        authConfig
      )
      .then((respuesta) => {
        setDataDoctor(respuesta.data);
        console.log(respuesta);
      });
  }, []);
  return (
    <div className='d-flex justify-content-center mt-5 pt-3'>
      <div className='card mx-3 mb-4 mt-2' style={{ width: '400px' }}>
        <div className='text-center mt-3'>
          <span className='text-primary'> id: </span> {dataDoctor.id}
        </div>
        <div className='text-center'>
          <span className='text-primary'> Name </span>: {dataDoctor.name}
        </div>

        <div className='text-center'>
          <span className='text-primary'> Last Name </span>:{' '}
          {dataDoctor.last_name}
        </div>
        <div className='text-center'>
          <span className='text-primary'> Email </span>: {dataDoctor.email}
        </div>

        <div className='text-center'>
          <span className='text-primary'> Telephone </span>:{' '}
          {dataDoctor.telephone}
        </div>

        <div className='text-center'>
          <span className='text-primary'>Gender: </span>:{dataDoctor.email}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Date of Birth: </span>:{' '}
          {dataDoctor.birth_date}
        </div>

        <div className='text-center'>
          <span className='text-primary'>DNI </span>: {dataDoctor.dni}
        </div>

        <div className='text-center'>
          <span className='text-primary'>Number School </span>:
          {dataDoctor.number_school}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Address</span>: {dataDoctor.address}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Registration User </span>:{' '}
          {dataDoctor.register_user}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Modification User </span>:{' '}
          {dataDoctor.modification_user}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Register Date </span>:{' '}
          {dataDoctor.register_date}
        </div>
        <div className='text-center mb-3'>
          <span className='text-primary'>Modification Date </span>:{' '}
          {dataDoctor.modification_date}
        </div>
      </div>
    </div>
  );
}
