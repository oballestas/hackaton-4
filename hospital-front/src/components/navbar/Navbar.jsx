import { useState } from 'react';
import { Link } from 'react-router-dom';

export function Navbar() {
  let token = localStorage.getItem('token');
  let [myToken, setMyToken] = useState(token);
  return (
    <div>
      <nav className='navbar-dark bg-dark static-top'>
        <div>
          {!myToken ? (
            <div className='container-fluid d-flex'>
              <div className='nav-item mt-1'>
                <Link className='navbar-brand' to='/'>
                  Hospital
                </Link>
              </div>
              <div className='nav-item'>
                <Link className='nav-link text-white' to='/login'>
                  Login
                </Link>
              </div>
            </div>
          ) : (
            <div className='container-fluid d-flex'>
              <div className='nav-item mt-1'>
                <Link className='navbar-brand' to='/'>
                  Hospital
                </Link>
              </div>
              <div className='nav-item'>
                <Link className='nav-link text-white' to='/doctors'>
                  Doctors
                </Link>
              </div>
              <div className='nav-item'>
                <Link className='nav-link text-white' to='/patients'>
                  Patients
                </Link>
              </div>
              <div className='nav-item'>
                <Link className='nav-link text-white' to='/schedule'>
                  Schedule
                </Link>
              </div>
              <div className='nav-item'>
                <Link className='nav-link text-white' to='/appointments'>
                  Appointments
                </Link>
              </div>

              <div className='nav-item'>
                <Link
                  to='/'
                  onClick={() => {
                    localStorage.removeItem('token');
                    window.location.reload();
                    setMyToken(localStorage.getItem('token'));
                  }}
                  className='nav-link text-white'
                >
                  Logout
                </Link>
              </div>
            </div>
          )}
        </div>
      </nav>
    </div>
  );
}
