import axios from 'axios';
import { useState } from 'react';
import { Link } from 'react-router-dom';

export function PatientRow({ id, name, last_name, dni }) {
  let [patientId, setpatientId] = useState(id);
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };
  function deleteDoctor() {
    axios
      .delete(`http://127.0.0.1:8000/api/patients/${patientId}`, authConfig)
      .then((respuesta) => {
        console.log(respuesta);
        window.location.reload();
      });
  }

  return (
    <tr key={id}>
      <th scope='row'>
        <Link to={`/patients/detail/${patientId}`}>{id}</Link>
      </th>
      <td>{name}</td>
      <td>{last_name}</td>
      <td>{dni}</td>

      <td>
        <Link className='btn btn-primary mx-3' to={`/patients/edit/${id}`}>
          Edit
        </Link>

        <button
          className='btn btn-danger'
          type='submit'
          onClick={() => {
            deleteDoctor();
          }}
        >
          Delete
        </button>
      </td>
    </tr>
  );
}
