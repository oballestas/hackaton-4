import { Link } from 'react-router-dom';
import { DoctorRow } from './doctorRow';

export function DoctorTable(dataDoctor) {
  return (
    <div>
      <div className='row mt-5 d-flex justify-content-center'>
        <div className='d-flex mt-5 px-5 pt-4 justify-content-center'>
          <div className='text-center mt-2 ms-auto'>
            <Link className='btn btn-dark' to='/doctors/add'>
              Add
            </Link>
          </div>
          <div className='mx-5'></div>
        </div>
        <h2 className='text-center text-success mb-5 pt-3'>Doctors</h2>
        <div className='col-6'>
          <table className='table caption-top'>
            <thead className='text-center'>
              <tr>
                <th scope='col'>id</th>
                <th scope='col'>Name</th>
                <th scope='col'>Last name</th>
                <th scope='col'>DNI</th>
                <th scope='col'>Actions</th>
              </tr>
            </thead>
            <tbody>
              {dataDoctor?.results ? (
                dataDoctor.results.map((doctor) => {
                  return <DoctorRow key={doctor.id} {...doctor} />;
                })
              ) : (
                <tr></tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
