import { Link } from 'react-router-dom';
import { ScheduleRow } from './scheduleRow';

export function ScheduleTable(dataSchedule) {
  return (
    <div>
      <div className='row mt-5 d-flex justify-content-center'>
        <div className='d-flex mt-5 px-5 pt-4 justify-content-center'>
          <div className='text-center mt-2 ms-auto'>
            <Link className='btn btn-dark' to='/schedule/add'>
              Add
            </Link>
          </div>
          <div className='mx-5'></div>
        </div>
        <h2 className='text-center text-success mb-5 pt-3'>Schedule</h2>
        <div className='col-6'>
          <table className='table caption-top'>
            <thead className='text-center'>
              <tr>
                <th scope='col'>id</th>
                <th scope='col'>Appointment Date</th>
                <th scope='col'>Start Time</th>
                <th scope='col'>End Time</th>
                <th scope='col'>Doctor</th>
                <th scope='col'>Actions</th>
              </tr>
            </thead>
            <tbody>
              {dataSchedule?.results ? (
                dataSchedule.results.map((schedule) => {
                  return <ScheduleRow key={schedule.id} {...schedule} />;
                })
              ) : (
                <tr></tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
