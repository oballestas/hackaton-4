import axios from 'axios';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

export function ScheduleRow({
  id,
  appointment_date,
  start_time,
  end_time,
  doctor,
}) {
  let [scheduleId, setScheduleId] = useState(id);
  let [doctorData, setDoctorData] = useState([]);
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };
  useEffect(() => {
    axios
      .get(`http://127.0.0.1:8000/api/doctors/${doctor}`, authConfig)
      .then((respuesta) => {
        setDoctorData(respuesta.data);
      });
  }, []);
  function deleteSchedule() {
    axios
      .delete(`http://127.0.0.1:8000/api/schedule/${scheduleId}`, authConfig)
      .then((respuesta) => {
        console.log(respuesta);
        window.location.reload();
      });
  }

  return (
    <tr key={id}>
      <th scope='row'>
        <Link to={`/schedule/detail/${scheduleId}`}>{id}</Link>
      </th>
      <td>{appointment_date}</td>
      <td>{start_time}</td>
      <td>{end_time}</td>
      <td>
        {doctorData.name} {doctorData.last_name}
      </td>

      <td>
        <Link className='btn btn-primary mx-3' to={`/schedule/edit/${id}`}>
          Edit
        </Link>

        <button
          className='btn btn-danger'
          type='submit'
          onClick={() => {
            deleteSchedule();
          }}
        >
          Delete
        </button>
      </td>
    </tr>
  );
}
