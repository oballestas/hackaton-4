import axios from 'axios';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

export function PatientDetail() {
  let urlParameters = useParams();
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };

  let [dataPatient, setDataPatient] = useState([]);
  useEffect(() => {
    axios
      .get(
        `http://127.0.0.1:8000/api/patients/${urlParameters.patientId}`,
        authConfig
      )
      .then((respuesta) => {
        setDataPatient(respuesta.data);
        console.log(respuesta);
      });
  }, []);
  return (
    <div className='d-flex justify-content-center mt-5 pt-3'>
      <div className='card mx-3 mb-4 mt-2' style={{ width: '400px' }}>
        <div className='text-center mt-3'>
          <span className='text-primary'> id: </span> {dataPatient.id}
        </div>
        <div className='text-center'>
          <span className='text-primary'> Name </span>: {dataPatient.name}
        </div>

        <div className='text-center'>
          <span className='text-primary'> Last Name </span>:{' '}
          {dataPatient.last_name}
        </div>

        <div className='text-center'>
          <span className='text-primary'> Telephone </span>:{' '}
          {dataPatient.telephone}
        </div>

        <div className='text-center'>
          <span className='text-primary'>Gender: </span>:{dataPatient.email}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Date of Birth: </span>:{' '}
          {dataPatient.birth_date}
        </div>

        <div className='text-center'>
          <span className='text-primary'>DNI </span>: {dataPatient.dni}
        </div>

        <div className='text-center'>
          <span className='text-primary'>Address</span>: {dataPatient.address}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Registration User </span>:{' '}
          {dataPatient.register_user}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Modification User </span>:{' '}
          {dataPatient.modification_user}
        </div>
        <div className='text-center'>
          <span className='text-primary'>Register Date </span>:{' '}
          {dataPatient.register_date}
        </div>
        <div className='text-center mb-3'>
          <span className='text-primary'>Modification Date </span>:{' '}
          {dataPatient.modification_date}
        </div>
      </div>
    </div>
  );
}
