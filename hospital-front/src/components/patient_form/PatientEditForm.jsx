import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

export function PatientEditForm() {
  let urlParameters = useParams();
  const navigate = useNavigate();

  let [dataPatient, setDataPatient] = useState([]);
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };
  useEffect(() => {
    axios
      .get(
        `http://127.0.0.1:8000/api/patients/${urlParameters.patientId}`,
        authConfig
      )
      .then((respuesta) => {
        setDataPatient(respuesta.data);
        //console.log(respuesta);
      });
  }, []);

  function putData(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());

    axios
      .put(
        `http://127.0.0.1:8000/api/patients/${urlParameters.patientId}/`,
        values,
        authConfig
      )
      .then((respuesta) => {
        console.log(respuesta);
        navigate('/patients');
      });
  }

  return (
    <div className='d-flex justify-content-center mt-5'>
      <form
        onSubmit={(e) => {
          putData(e);
        }}
      >
        <h2 className='text-success'>Edit Patient Profile</h2>
        <div className='row mt-5'>
          <div className='mb-3 col-6'>
            <label className='form-label'>Name</label>
            <input
              type='text'
              className='form-control'
              id='name'
              name='name'
              defaultValue={dataPatient.name}
            />
          </div>

          <div className='mb-3 col-6'>
            <label className='form-label'>Last Name</label>
            <input
              type='text'
              className='form-control'
              id='last_name'
              name='last_name'
              defaultValue={dataPatient.last_name}
            />
          </div>
        </div>

        <div className='row'>
          <div className='mb-3 col-5'>
            <label className='form-label'>Telephone</label>
            <input
              type='text'
              className='form-control'
              id='telephone'
              name='telephone'
              defaultValue={dataPatient.telephone}
            />
          </div>
          <div className='mb-3 col-7'>
            <label className='form-label'>DNI</label>
            <input
              type='text'
              className='form-control'
              id='dni'
              name='dni'
              defaultValue={dataPatient.dni}
            />
          </div>
        </div>

        <div className='row'>
          <div className='mb-3 col-5'>
            <label className='form-label'>Gender</label>
            <input
              type='text'
              className='form-control'
              id='gender'
              name='gender'
              defaultValue={dataPatient.gender}
            />
          </div>
          <div className='mb-3 col-7'>
            <label className='form-label'>Date of Birth </label>
            <input
              type='date'
              className='form-control'
              id='birth_date'
              name='birth_date'
              defaultValue={dataPatient.birth_date}
            />
          </div>
        </div>

        <div className='row'>
          <div className='mb-3 col-9'>
            <label className='form-label'>Address</label>
            <input
              type='text'
              className='form-control'
              id='address'
              name='address'
              defaultValue={dataPatient.address}
            />
          </div>

          <div className='col-3'>
            <label className='form-label'>State</label>
            <select className='form-select bg-white' name='active'>
              <option value={true}>Active</option>
              <option value={false}>Not Active</option>
            </select>
          </div>
        </div>

        <div className='mb-3 '>
          <label className='form-label'>Register User</label>
          <input
            type='text'
            className='form-control'
            id='register_user'
            name='register_user'
            defaultValue={dataPatient.register_user}
            readOnly
          />
        </div>

        <div className='mb-3'>
          <label className='form-label'>Modification User</label>
          <input
            type='text'
            className='form-control'
            id='modification_user'
            name='modification_user'
            defaultValue={dataPatient.modification_user}
          />
        </div>

        <div className='mb-4'>
          <button type='submit' className='btn btn-success mb-3 form-control'>
            Save
          </button>
        </div>
      </form>
    </div>
  );
}
