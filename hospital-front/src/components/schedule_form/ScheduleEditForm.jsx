import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

export function ScheduleEditForm() {
  let urlParameters = useParams();
  const navigate = useNavigate();
  let [doctorData, setDoctorData] = useState([]);

  let [dataSchedule, setDataSchedule] = useState([]);
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get(`http://127.0.0.1:8000/api/doctors/`, authConfig)
      .then((respuesta) => {
        setDoctorData(respuesta.data);
      });
  }, []);

  useEffect(() => {
    axios
      .get(
        `http://127.0.0.1:8000/api/schedule/${urlParameters.scheduleId}`,
        authConfig
      )
      .then((respuesta) => {
        setDataSchedule(respuesta.data);
      });
  }, []);

  function putData(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());

    axios
      .put(
        `http://127.0.0.1:8000/api/schedule/${urlParameters.scheduleId}/`,
        values,
        authConfig
      )
      .then((respuesta) => {
        console.log(respuesta);
        navigate('/schedule');
      });
  }

  return (
    <div className='d-flex justify-content-center mt-5'>
      <form
        onSubmit={(e) => {
          putData(e);
        }}
      >
        <h2 className='text-success'>Edit Schedule</h2>
        <div className='row'>
          <div className='mb-3 col-7'>
            <label className='form-label'>Appointment Date</label>
            <input
              type='date'
              className='form-control'
              id='appointment_date'
              name='appointment_date'
              defaultValue={dataSchedule.appointment_date}
            />
          </div>
          <div className='col-5'>
            <label className='form-label'>State</label>
            <select className='form-select bg-white' name='active'>
              <option value={true}>Active</option>
              <option value={false}>Not Active</option>
            </select>
          </div>
        </div>

        <div className='row'>
          <div className='mb-3 col-6'>
            <label className='form-label'>Start Time</label>
            <input
              type='time'
              className='form-control'
              id='start_time'
              name='start_time'
              defaultValue={dataSchedule.start_time}
            />
          </div>
          <div className='mb-3 col-6'>
            <label className='form-label'>End Time</label>
            <input
              type='time'
              className='form-control'
              id='end_time'
              name='end_time'
              defaultValue={dataSchedule.end_time}
            />
          </div>
        </div>

        <div className='mb-3'>
          <label className='form-label'>Doctor</label>
          <select className='form-select bg-white' name='doctor'>
            {doctorData?.results
              ? doctorData.results.map((doctor) => {
                  return (
                    <option value={doctor.id} key={doctor.id}>
                      {doctor.name} {doctor.last_name}
                    </option>
                  );
                })
              : 'Loading...'}
          </select>
        </div>

        <div className='mb-3 '>
          <label className='form-label'>Register User</label>
          <input
            type='text'
            className='form-control'
            id='register_user'
            name='register_user'
            defaultValue={dataSchedule.register_user}
            readOnly
          />
        </div>

        <div className='mb-3'>
          <label className='form-label'>Modification User</label>
          <input
            type='text'
            className='form-control'
            id='modification_user'
            name='modification_user'
            defaultValue={dataSchedule.modification_user}
          />
        </div>

        <div className='mb-4'>
          <button type='submit' className='btn btn-success mb-3 form-control'>
            Add Schedule
          </button>
        </div>
      </form>
    </div>
  );
}
