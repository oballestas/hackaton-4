import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

export function ScheduleForm() {
  const navigate = useNavigate();
  let [doctorData, setDoctorData] = useState([]);
  let token = localStorage.getItem('token');
  const authConfig = {
    headers: { Authorization: `Bearer ${token}` },
  };

  useEffect(() => {
    axios
      .get(`http://127.0.0.1:8000/api/doctors/`, authConfig)
      .then((respuesta) => {
        //console.log(respuesta.data);
        setDoctorData(respuesta.data);
      });
  }, []);

  function postData(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());
    let token = localStorage.getItem('token');
    const authConfig = {
      headers: { Authorization: `Bearer ${token}` },
    };

    axios
      .post('http://127.0.0.1:8000/api/schedule/', values, authConfig)
      .then((respuesta) => {
        console.log(respuesta);
        navigate('/schedule');
      });
  }

  return (
    <div className='d-flex justify-content-center mt-5'>
      <form
        onSubmit={(e) => {
          postData(e);
        }}
      >
        <h2 className='text-success mb-3'>Add Schedule</h2>

        <div className='row'>
          <div className='mb-3 col-7'>
            <label className='form-label'>Appointment Date</label>
            <input
              type='date'
              className='form-control'
              id='appointment_date'
              name='appointment_date'
            />
          </div>
          <div className='col-5'>
            <label className='form-label'>State</label>
            <select className='form-select bg-white' name='active'>
              <option value={true}>Active</option>
              <option value={false}>Not Active</option>
            </select>
          </div>
        </div>

        <div className='row'>
          <div className='mb-3 col-6'>
            <label className='form-label'>Start Time</label>
            <input
              type='time'
              className='form-control'
              id='start_time'
              name='start_time'
            />
          </div>
          <div className='mb-3 col-6'>
            <label className='form-label'>End Time</label>
            <input
              type='time'
              className='form-control'
              id='end_time'
              name='end_time'
            />
          </div>
        </div>

        <div className='mb-3'>
          <label className='form-label'>Doctor</label>
          <select className='form-select bg-white' name='doctor'>
            {doctorData?.results
              ? doctorData.results.map((doctor) => {
                  return (
                    <option value={doctor.id} key={doctor.id}>
                      {doctor.name} {doctor.last_name}
                    </option>
                  );
                })
              : 'Loading...'}
          </select>
        </div>

        <div className='mb-3 '>
          <label className='form-label'>Register User</label>
          <input
            type='text'
            className='form-control'
            id='register_user'
            name='register_user'
          />
        </div>

        <div className='mb-3'>
          <label className='form-label'>Modification User</label>
          <input
            type='text'
            className='form-control'
            id='modification_user'
            name='modification_user'
          />
        </div>

        <div className='mb-4'>
          <button type='submit' className='btn btn-success mb-3 form-control'>
            Add Schedule
          </button>
        </div>
      </form>
    </div>
  );
}
