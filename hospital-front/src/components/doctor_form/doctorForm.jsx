import axios from 'axios';
import { useNavigate } from 'react-router-dom';

export function DoctorForm() {
  const navigate = useNavigate();
  function postData(e) {
    e.preventDefault();
    const form = new FormData(e.target);
    const values = Object.fromEntries(form.entries());
    let token = localStorage.getItem('token');
    const authConfig = {
      headers: { Authorization: `Bearer ${token}` },
    };

    axios
      .post('http://127.0.0.1:8000/api/doctors/', values, authConfig)
      .then((respuesta) => {
        console.log(respuesta);
        navigate('/doctors');
      });
  }

  return (
    <div className='d-flex justify-content-center mt-5'>
      <form
        onSubmit={(e) => {
          postData(e);
        }}
      >
        <h2 className='text-success'>Create Doctor Profile</h2>
        <div className='row mt-5'>
          <div className='mb-3 col-6'>
            <label className='form-label'>Name</label>
            <input type='text' className='form-control' id='name' name='name' />
          </div>

          <div className='mb-3 col-6'>
            <label className='form-label'>Last Name</label>
            <input
              type='text'
              className='form-control'
              id='last_name'
              name='last_name'
            />
          </div>
        </div>

        <div className='row'>
          <div className='mb-3 col-8'>
            <label className='form-label'>Email</label>
            <input
              type='email'
              className='form-control'
              id='email'
              name='email'
            />
          </div>

          <div className='mb-3 col-4'>
            <label className='form-label'>Telephone</label>
            <input
              type='text'
              className='form-control'
              id='telephone'
              name='telephone'
            />
          </div>
        </div>

        <div className='row'>
          <div className='mb-3 col-5'>
            <label className='form-label'>Gender</label>
            <input
              type='text'
              className='form-control'
              id='gender'
              name='gender'
            />
          </div>
          <div className='mb-3 col-7'>
            <label className='form-label'>Date of Birth</label>
            <input
              type='date'
              className='form-control'
              id='birth_date'
              name='birth_date'
            />
          </div>
        </div>

        <div className='row'>
          <div className='mb-3 col-6'>
            <label className='form-label'>DNI</label>
            <input type='text' className='form-control' id='dni' name='dni' />
          </div>

          <div className='mb-3 col-6'>
            <label className='form-label'>Number School </label>
            <input
              type='text'
              className='form-control'
              id='number_school'
              name='number_school'
            />
          </div>
        </div>

        <div className='row'>
          <div className='mb-3 col-9'>
            <label className='form-label'>Address</label>
            <input
              type='text'
              className='form-control'
              id='address'
              name='address'
            />
          </div>

          <div className='col-3'>
            <label className='form-label'>State</label>
            <select className='form-select bg-white' name='active'>
              <option value={true}>Active</option>
              <option value={false}>Not Active</option>
            </select>
          </div>
        </div>

        <div className='mb-3 '>
          <label className='form-label'>Register User</label>
          <input
            type='text'
            className='form-control'
            id='register_user'
            name='register_user'
          />
        </div>

        <div className='mb-3'>
          <label className='form-label'>Modification User</label>
          <input
            type='text'
            className='form-control'
            id='modification_user'
            name='modification_user'
          />
        </div>

        <div className='mb-4'>
          <button type='submit' className='btn btn-success mb-3 form-control'>
            Create Profile
          </button>
        </div>
      </form>
    </div>
  );
}
